﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#region
/*
   ITEM IDs
[ Food ][ Toy ][ Clean ]
   0       4       8
   1       5       9
   2       6       10
   3       7       11
*/
#endregion

public class ItemUsed : MonoBehaviour
{
    public int ItemID;
    //public int itemAmount;

    public void GetFood()
    {
        switch (ItemID)
        {
            case 0:
                {
                    CatStats.Hunger += 10;
                    CatStats.CatMood -= 10;
                    CatStats.Clean -= 5;
                    SoundManager.instance.PlayMeow();
                }
                break;
            case 1:
                if (ItemAmount.foodAmount1 > 0)
                {
                    CatStats.Hunger += 10;
                    ItemAmount.foodAmount1 -= 1;
                    SoundManager.instance.PlayMeow();
                }
                    
                break;
            case 2:
                if (ItemAmount.foodAmount2 > 0)
                {
                    CatStats.Hunger += 10;
                    CatStats.CatMood += 5;
                    ItemAmount.foodAmount2 -= 1;
                    SoundManager.instance.PlayMeow();
                }
                break;
            case 3:
                if (ItemAmount.foodAmount3 > 0)
                { 
                    CatStats.Hunger += 20;
                    CatStats.CatMood += 5;
                    CatStats.Clean -= 3;
                    ItemAmount.foodAmount3 -= 1;
                    SoundManager.instance.PlayMeow();
                }
                //MINIGAME
                break;
        }
    }

    public void GetToy()
    {
        switch (ItemID)
        {
            case 4:
                if (MoneyScript.SDMoney >= 0)
                {
                   CatStats.CatMood += 2;
                    SoundManager.instance.PlayScreaming();
                    // MINIGAME
                }
                    
                break;
            case 5:
                if (ItemAmount.toyAmount1 > 0)
                {
                    CatStats.CatMood += 5;
                    ItemAmount.toyAmount1 -= 1;
                    SoundManager.instance.PlayMeowing();
                }
                break;
            case 6:
                if (ItemAmount.toyAmount2 > 0)
                {
                    CatStats.CatMood += 10;
                    CatStats.Clean -= 3;
                    CatStats.Hunger -= 5;
                    ItemAmount.toyAmount2 -= 1;
                    SoundManager.instance.PlayMeow();
                }
                break;
            case 7:
                if (ItemAmount.toyAmount3 > 0)
                {
                    CatStats.CatMood += 20;
                    CatStats.Hunger -= 8;
                    CatStats.Clean -= 5;
                    ItemAmount.toyAmount3 -= 1;
                    SoundManager.instance.PlayMeowing();
                }
                break;
        }       
    }

    public void GetClean()
    {
        switch (ItemID)
        {
            case 8:
                if (MoneyScript.SDMoney >= 5)
                {
                    CatStats.Clean += 20;
                    CatStats.CatMood -= 10;
                    MoneyScript.SDMoney -= 5;
                    SoundManager.instance.PlayScreaming();
                }

                break;
            case 9:
                if (ItemAmount.cAmount1 > 0)
                {
                    CatStats.Clean += 20;
                    CatStats.CatMood += 5;
                    ItemAmount.cAmount1 -= 1;
                    SoundManager.instance.PlayScreaming();
                }
                break;
            case 10:
                if (ItemAmount.cAmount2 > 0)
                {
                    CatStats.Clean += 10;
                    CatStats.Hunger -= 5;
                    CatStats.CatMood += 3;
                    ItemAmount.cAmount2 -= 1;
                    SoundManager.instance.PlayMeowing();
                }
                break;
            case 11:
                if (ItemAmount.cAmount3 > 0)
                {
                    CatStats.Clean += 50;
                    CatStats.CatMood -= 20;
                    ItemAmount.cAmount3 -= 1;
                    SoundManager.instance.PlayMeow();
                }
                break;
        }

    }


}
