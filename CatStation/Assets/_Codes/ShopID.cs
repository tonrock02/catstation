﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopID : MonoBehaviour
{
    public int shopID;

    public void BuyItems()
    {
        switch (shopID)
        {
            // Food
            case 0:
                if (MoneyScript.SDMoney >= 10)
                {                   
                    ItemAmount.foodAmount1 += 1;
                    MoneyScript.SDMoney -= 10;
                }
                break;
            case 1:
                if (MoneyScript.SDMoney >= 15)
                {
                    ItemAmount.foodAmount2 += 1;
                    MoneyScript.SDMoney -= 15;
                }
                break;
            case 2:
                if (MoneyScript.SDMoney >= 20)
                {
                    ItemAmount.foodAmount3 += 1;
                    MoneyScript.SDMoney -= 20;
                }
                break;

                // Toy
            case 3:
                if (MoneyScript.SDMoney >= 10)
                {
                    ItemAmount.toyAmount1 += 1;
                    MoneyScript.SDMoney -= 10;
                }
                break;
            case 4:
                if (MoneyScript.SDMoney >= 20)
                {
                    ItemAmount.toyAmount2 += 1;
                    MoneyScript.SDMoney -= 20;
                }
                break;
            case 5:
                if (MoneyScript.SDMoney >= 30)
                {
                    ItemAmount.toyAmount3 += 1;
                    MoneyScript.SDMoney -= 30;
                }
                break;

                // Clean Item
            case 6:
                if (MoneyScript.SDMoney >= 5)
                {
                    ItemAmount.cAmount1 += 1;
                    MoneyScript.SDMoney -= 5;
                }
                break;
            case 7:
                if (MoneyScript.SDMoney >= 10)
                {
                    ItemAmount.cAmount2 += 1;
                    MoneyScript.SDMoney -= 10;
                }
                break;
            case 8:
                if (MoneyScript.SDMoney >= 25)
                {
                    ItemAmount.cAmount3 += 1;
                    MoneyScript.SDMoney -= 25;
                }
                break;
        }
    }
 
}
