﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyScript : MonoBehaviour
{
    public static float SDMoney = 100;
    public Text textbox;
    private float MoneyRegen = 5;
    
    // Update is called once per frame
    void Update()
    {
        textbox.text = "Money: " + Mathf.Round(SDMoney);
        MoneyGenerateOvertime();
    }

    void MoneyGenerateOvertime()
    {
        SDMoney += MoneyRegen * Time.deltaTime;
    }
}
