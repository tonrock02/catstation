﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemAmount : MonoBehaviour
{
    public Text foodItem1;
    public Text foodItem2;
    public Text foodItem3;

    public Text toyItem1;
    public Text toyItem2;
    public Text toyItem3;

    public Text cItem1;
    public Text cItem2;
    public Text cItem3;

    public static int foodAmount1 = 5;
    public static int foodAmount2 = 3;
    public static int foodAmount3 = 2;

    public static int toyAmount1 = 5;
    public static int toyAmount2 = 3;
    public static int toyAmount3 = 2;

    public static int cAmount1 = 5;
    public static int cAmount2 = 3;
    public static int cAmount3 = 2;


    // Update is called once per frame
    void Update()
    {
        Amount();
    }

    void Amount()
    {
        foodItem1.text = "" + foodAmount1;
        foodItem2.text = "" + foodAmount2;
        foodItem3.text = "" + foodAmount3;

        toyItem1.text = "" + toyAmount1;
        toyItem2.text = "" + toyAmount2;
        toyItem3.text = "" + toyAmount3;

        cItem1.text = "" + cAmount1;
        cItem2.text = "" + cAmount2;
        cItem3.text = "" + cAmount3;
    }
}
