﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadSceneGameplay : MonoBehaviour
{
    public void GotoGameplay()
    {
        SceneManager.UnloadSceneAsync("MainMenu");
        SceneManager.LoadScene("GamePlay");
    }
}
