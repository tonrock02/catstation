﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip meowSound;
    public AudioClip meowingSound;
    public AudioClip purringmeowSound;
    public AudioClip screamingSound;
    AudioSource m_source;
    public static SoundManager instance;
    // Start is called before the first frame update

    void Awake()
    {
        if (instance == null)
            instance = this;
        m_source = GetComponent<AudioSource>();
    }

    public void PlayMeow()
    {
        m_source.PlayOneShot(meowSound, m_source.volume);
    }

    public void PlayMeowing()
    {
        m_source.PlayOneShot(meowingSound, m_source.volume);
    }

    public void PlayPurringmeow()
    {
        m_source.PlayOneShot(purringmeowSound, m_source.volume);
    }

    public void PlayScreaming()
    {
        m_source.PlayOneShot(screamingSound, m_source.volume);
    }

}
