﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGame2Manager : MonoBehaviour
{
    public GameObject Panel2;
    public FoodDrop[] fooddrops;

    public static float TimeUp = 100;
    public float TimeUpOvertime;
    public Slider TimeUpBar;

    void OnDisable()
    {
        Debug.Log("PrintOnDisable: script was disabled");
    }

    void OnEnable()
    {
        for (int i = 0; i < 3; i++)
        {
            fooddrops[i].ResetMiniGame();
        }
        CatStats.PauseGame = true;
        Debug.Log("PrintOnEnable: script was enabled");
    }
    // Start is called before the first frame update
    void Start()
    {
        TimeUpBar.maxValue = TimeUp;
        updateUI();
    }

    // Update is called once per frame
    void Update()
    {
        Calculate();
        if (DragFingerMove.Instance.currentScore >= 10 && TimeUp <= 0f)
        {
            CatStats.Hunger += 15;
            DragFingerMove.Instance.currentScore = 0;
            TimeUp = 100;
            CatStats.PauseGame = false;
            SoundManager.instance.PlayMeow();
            Panel2.gameObject.SetActive(false);
            
        }
        if (DragFingerMove.Instance.currentScore < 10 && TimeUp <= 0f)
        {
            DragFingerMove.Instance.currentScore = 0;
            TimeUp = 100;
            CatStats.PauseGame = false;
            SoundManager.instance.PlayScreaming();
            Panel2.gameObject.SetActive(false);
        }
    }

    private void Calculate()
    {
        TimeUp -= TimeUpOvertime * Time.deltaTime;
        updateUI();
    }

    private void updateUI()
    {
        TimeUp = Mathf.Clamp(TimeUp, 0, 100f);

        TimeUpBar.value = TimeUp;
    }
}
