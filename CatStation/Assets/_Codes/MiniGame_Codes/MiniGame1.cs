﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGame1 : MonoBehaviour
{
    public static float TimeUp = 100;
    public float TimeUpOvertime;

    public Slider TimeUpBar;

    public static int TapCount;
    public static int currentTapCount;
    //-------UI-------
    public GameObject Panel;
    public Text counttabText;

    public GameObject Cat;
    public GameObject CatHappy;
    void OnDisable()
    {
        Debug.Log("PrintOnDisable: script was disabled");
    }

    void OnEnable()
    {
        CatStats.PauseGame = true;
        Debug.Log("PrintOnEnable: script was enabled");
    }
    // Start is called before the first frame update
    void Start()
    {
        TimeUpBar.maxValue = TimeUp;
        updateUI();
    }

    // Update is called once per frame
    void Update()
    {
        Calculate();
        if(TapCount >= 30)
        {
            Cat.SetActive(false);
            CatHappy.SetActive(true);
        }
        if (TapCount >= 30 && TimeUp <= 0f)
        {
            CatStats.CatMood += 15;
            TapCount = 0;
            TimeUp = 100;
            CatStats.PauseGame = false;
            Cat.SetActive(true);
            CatHappy.SetActive(false);
            SoundManager.instance.PlayMeow();
            Panel.gameObject.SetActive(false);
        }
        if (TapCount < 30 && TimeUp <= 0f)
        {
            TapCount = 0;
            TimeUp = 100;
            CatStats.PauseGame = false;
            Cat.SetActive(true);
            CatHappy.SetActive(false);
            SoundManager.instance.PlayScreaming();
            Panel.gameObject.SetActive(false);
        }
        counttabText.text = TapCount.ToString();
    }

    private void Calculate()
    {
        TimeUp -= TimeUpOvertime * Time.deltaTime;
        currentTapCount = TapCount;
        updateUI();
    }

    private void updateUI()
    {
        TimeUp = Mathf.Clamp(TimeUp, 0, 100f);

        TimeUpBar.value = TimeUp;
    }
}
