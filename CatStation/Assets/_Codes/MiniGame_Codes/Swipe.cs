﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Swipe : MonoBehaviour
{
    private Touch touch;

    private Vector2 beginTouchPosition, endTouchPosition;

    public Text SwipeCount;
    private int currentSwipeCount;
    // Update is called once per frame
    void Update()
    {
        SwipeCount.text = currentSwipeCount.ToString();
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    beginTouchPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    endTouchPosition = touch.position;

                    if (beginTouchPosition != endTouchPosition)
                        currentSwipeCount += 1;
                    break;
            }
        }
    }
}
