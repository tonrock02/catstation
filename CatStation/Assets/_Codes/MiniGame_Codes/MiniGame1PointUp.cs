﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGame1PointUp : MonoBehaviour
{
    public void GetPoint()
    {
        MiniGame1.TapCount += 1;
    }
}
