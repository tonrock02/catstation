﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGame3Manager : MonoBehaviour
{
    public static float TimeUp = 100;
    public float TimeUpOvertime;

    public Slider TimeUpBar;

    //-------UI-------
    public GameObject Panel;

    //------Swipe-----
    private Touch touch;
    private Vector2 beginTouchPosition, endTouchPosition;
    public Text SwipeCount;
    private int currentSwipeCount;

    public GameObject Cat2;
    public GameObject CatHappy2;

    void OnDisable()
    {
        Debug.Log("PrintOnDisable: script was disabled");
    }

    void OnEnable()
    {
        CatStats.PauseGame = true;
        Debug.Log("PrintOnEnable: script was enabled");
    }
    // Start is called before the first frame update
    void Start()
    {
        TimeUpBar.maxValue = TimeUp;
        updateUI();
    }

    // Update is called once per frame
    void Update()
    {
        Calculate();
        SwipeCount.text = currentSwipeCount.ToString();
        if (currentSwipeCount >= 15)
        {
            Cat2.SetActive(false);
            CatHappy2.SetActive(true);
        }
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    beginTouchPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    endTouchPosition = touch.position;

                    if (beginTouchPosition != endTouchPosition)
                        currentSwipeCount += 1;
                        SoundManager.instance.PlayMeow();
                    break;
            }
        }

        if (currentSwipeCount >= 15 && TimeUp <= 0f)
        {
            CatStats.Clean += 15;
            currentSwipeCount = 0;
            TimeUp = 100;
            CatStats.PauseGame = false;
            Cat2.SetActive(true);
            CatHappy2.SetActive(false);
            SoundManager.instance.PlayMeow();
            Panel.gameObject.SetActive(false);
        }
        if (currentSwipeCount < 15 && TimeUp <= 0f)
        {
            currentSwipeCount = 0;
            TimeUp = 100;
            CatStats.PauseGame = false;
            Cat2.SetActive(true);
            CatHappy2.SetActive(false);
            SoundManager.instance.PlayScreaming();
            Panel.gameObject.SetActive(false);
        }
    }

    private void updateUI()
    {
        TimeUp = Mathf.Clamp(TimeUp, 0, 100f);

        TimeUpBar.value = TimeUp;
    }

    private void Calculate()
    {
        TimeUp -= TimeUpOvertime * Time.deltaTime;
        updateUI();
    }
}
