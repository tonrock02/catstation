﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class FoodDrop : MonoBehaviour
{
    public static FoodDrop Instance { get; set; }

    public float speed;
    public float MaxDestroyRange;
    public float spawnHeight;
    private Rigidbody2D rb2;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        ResetMiniGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < MaxDestroyRange)
        {
            transform.position = new Vector3(Random.Range(110, 380), spawnHeight, transform.position.z);
            speed = Random.Range(80, 300);
            rb2 = this.GetComponent<Rigidbody2D>();
            rb2.velocity = new Vector2(0, -speed);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag.Contains("PlayerFoodDrop"))
        {
            SoundManager.instance.PlayMeow();
            transform.position = new Vector3(Random.Range(110, 380), spawnHeight, transform.position.z);
            speed = Random.Range(80, 300);
            rb2 = this.GetComponent<Rigidbody2D>();
            rb2.velocity = new Vector2(0, -speed);
        }
        if (other.collider.tag.Contains("LimitZone"))
        {
            transform.position = new Vector3(Random.Range(110, 380), spawnHeight, transform.position.z);
            speed = Random.Range(80, 300);
            rb2 = this.GetComponent<Rigidbody2D>();
            rb2.velocity = new Vector2(0, -speed);
        }
    }



    public void ResetMiniGame()
    {
        speed = Random.Range(80, 300);
        rb2 = this.GetComponent<Rigidbody2D>();
        rb2.velocity = new Vector2(0, -speed);
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }
}
