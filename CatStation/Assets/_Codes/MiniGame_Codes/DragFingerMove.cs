﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragFingerMove : MonoBehaviour
{
    public static DragFingerMove Instance { get; set; }

    public Text scoreText;
    public int currentScore;
    public float MovementSpeed;
    public float MaxLeft = 40f;
    public float MaxRight = 500f;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, MaxLeft, MaxRight), transform.position.y, transform.position.z);
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 TouchPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(TouchPosition.x * MovementSpeed * Time.deltaTime, 0, 0);
        }
        scoreText.text=currentScore.ToString();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag.Contains("FoodDrop"))
        {
            currentScore += 1;
        }
    }
}

