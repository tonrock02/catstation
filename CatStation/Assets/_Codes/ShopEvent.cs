﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShopEvent : MonoBehaviour
{
    public GameObject UI_Shop;

    public int foodPrice1 = 10;
    public int foodPrice2 = 15;
    public int foodPrice3 = 20;

    public int toyPrice1 = 5;
    public int toyPrice2 = 10;
    public int toyPrice3 = 15;

    public int cPrice1 = 5;
    public int cPrice2 = 10;
    public int cPrice3 = 25;

    public void ShopActive()
    {
        UI_Shop.SetActive(true);
    }

    public void ShopExit()
    {
        UI_Shop.SetActive(false);
    }

    public void buyfood()
    {
        
    }

}
