﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatStats : MonoBehaviour
{
    public static float Hunger = 100;
    public float HungerOvertime;

    public static float CatMood = 100;
    public float MoodOvertime;

    public static float Clean = 100;
    public float CleanOvertime;

    public float Point;
    public float PointOvertime;

    public Slider HungerBar;
    public Slider MoodBar;
    public Slider CleanBar;
    public Slider PointBar;

    public float minAmount = 10f;
    
    public static bool PauseGame;
    // Start is called before the first frame update
    void Start()
    {
        HungerBar.maxValue = Hunger;
        MoodBar.maxValue = CatMood;
        CleanBar.maxValue = Clean;
        PointBar.maxValue = Point;
        PauseGame = false;
        updateUI();
    }

    // Update is called once per frame
    void Update()
    {
        Calculatevalue();
    }

    public float DelayRegen = 0.25f;
    private void Calculatevalue()
    {
        if (PauseGame == false)
        {
            Hunger -= HungerOvertime * Time.deltaTime;
            CatMood -= MoodOvertime * Time.deltaTime;
            Clean -= CleanOvertime * Time.deltaTime;


            if (Hunger <= minAmount )
            {
                AnimControl.instance._Sad = true;
                Point -= PointOvertime * Time.deltaTime;
            }
            if ( CatMood <= minAmount )
            {
                AnimControl.instance._Sad = true;
                Point -= PointOvertime * Time.deltaTime;
            }
            if (Clean <= minAmount)
            {
                AnimControl.instance._Dirty = true;
                Point -= PointOvertime * Time.deltaTime;
            }
            if (Hunger > minAmount)
            {
                AnimControl.instance._Sad = false;
            }
            if (Clean > minAmount)
            {
                AnimControl.instance._Dirty = false;
            }
            if (CatMood > minAmount)
            {
                AnimControl.instance._Sad = false;
            }
            else
            {
                Point += PointOvertime * DelayRegen * Time.deltaTime;
            }
        }


        updateUI();
    }

    private void updateUI()
    {
        Hunger = Mathf.Clamp(Hunger, 0, 100f);
        CatMood = Mathf.Clamp(CatMood, 0, 100f);
        Clean = Mathf.Clamp(Clean, 0, 100f);
        Point = Mathf.Clamp(Point, 0, 100f);

        HungerBar.value = Hunger;
        MoodBar.value = CatMood;
        CleanBar.value = Clean;
        PointBar.value = Point;
    }
}
