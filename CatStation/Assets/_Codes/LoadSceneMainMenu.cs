﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneMainMenu : MonoBehaviour
{
    public void GotoMainMenu()
    {
        SceneManager.UnloadSceneAsync("GamePlay");
        SceneManager.LoadScene("MainMenu");
    }
}
