﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchScript : MonoBehaviour
{
    public static int _TapCount;
    public int _maxFinger;
    int _fingerCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _fingerCount = 0;
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase==TouchPhase.Began && _fingerCount < _maxFinger)
            {
                _TapCount++;
                Debug.Log(_TapCount);
            }
            _fingerCount++;
        }
    }
}
