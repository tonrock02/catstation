﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControl : MonoBehaviour
{
    public Animator animator;
    public static AnimControl instance;
    public bool _Dirty;
    public bool _Happy;
    public bool _Sad;
    void Start()
    {
        if (instance == null)
            instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (_Sad == true)
        {
            animator.SetInteger("Sad", 1);
        }
        if (_Sad == false)
        {
            animator.SetInteger("Sad", 0);
        }
        if (_Dirty == true)
        {
            animator.SetInteger("Dirty", 1);
        }
        if (_Dirty == false)
        {
            animator.SetInteger("Dirty", 0);
        }
        
       

    }
}
